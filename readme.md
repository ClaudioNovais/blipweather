# blip-weather

A small web app showing the weather using the Yahoo Api as data source.

There a set of tecnologies used in this web app, such as:
* Angular2
* Typescript
* Gulp

The roadmap can be seen in:
* [bitbucket.org/ClaudioNovais/blipweather/wiki/](https://bitbucket.org/ClaudioNovais/blipweather/wiki/Home)