/*jshint esversion: 6 */
var gulp = require("gulp"),
  del = require("del"),
  tsc = require("gulp-typescript"),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  minifyCSS = require('gulp-clean-css'),
  tsProject = tsc.createProject("tsconfig.json"),
  rename = require('gulp-rename'),
  autoprefixer = require('gulp-autoprefixer');


var config = {
  buildDir: 'build',
  npmDir: 'node_modules',
  bowerDir: 'bower_components',
  otherLibsDir: 'libs'
};

/**
 * Remove build directory.
 */
gulp.task('clean', (cb) => {
  return del([config.buildDir], cb);
});

/**
 * Compile TypeScript sources and create sourcemaps in build directory.
 */
gulp.task("compile", () => {
  var tsResult = gulp.src("src/**/*.ts")
    .pipe(sourcemaps.init())
    .pipe(tsc(tsProject));
  return tsResult.js
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(config.buildDir));
});

/**
 * Watch for changes in TypeScript, HTML and CSS files.
 */
gulp.task('watch', function () {
  gulp.watch(
    ["src/**/*.ts"], ['compile'])

    .on('change', e => {
      console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
    });
  gulp.watch(
    ["src/styles/*.scss"], ['sass'])

    .on('change', e => {
      console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
    });
  gulp.watch(
    ["src/**/*.html", "src/**/*.css"], ['resources'])
    .on('change', e => {
      console.log('Resource file ' + e.path + ' has been changed. Updating.');
    });
});

/**
 * Copy all resources that are not TypeScript files into build directory.
 */
gulp.task("resources", () => {
  return gulp.src(
    ["src/**/*",
      "systemjs.config.js",
      'node_modules/traceur/bin/traceur.js',

      /* Excluding... */
      "!**/*.ts",
      "!src/styles/*"])
    .pipe(gulp.dest(config.buildDir));
});

/**
 * Copy all required libraries into build directory.
 */
gulp.task("libs", () => {
  return gulp.src([

    'core-js/client/shim.min.js',
    'core-js/client/shim.min.js.map',
    'systemjs/dist/system-polyfills.js',
    'systemjs/dist/system.src.js',
    'reflect-metadata/Reflect.js',
    'rxjs/**',
    'zone.js/dist/**',
    '@angular/**',
    'angular2-notifications/**'

  ], { cwd: config.npmDir + "/**" })
    .pipe(gulp.dest(config.buildDir + "/" + config.npmDir));
});


/**
 * Copy all the frontend libraries into build directory.
 */
gulp.task("bower:libs", () => {
  return gulp.src([

    'jquery.min.js',
    'bootstrap.min.js'

  ], { cwd: config.bowerDir + "/**" })
    .pipe(gulp.dest(config.buildDir + "/" + config.otherLibsDir));
});

/**
 * Copy all css to build directory.
 */
gulp.task("css", () => {
  return gulp.src([

    'src/styles/*.css',

  ])
    .pipe(gulp.dest(config.buildDir + "/styles"));
});

/**
 * Process all SASS files to build directory
 */
gulp.task('sass', function () {
  return gulp.src('src/**/*.scss')
    .pipe(sass({
      errLogToConsole: true,
      style: 'compressed',
      includePaths: [
        'src/styles',
        config.bowerDir + '/bootstrap-sass/assets/stylesheets'
      ]
    }).on('error', sass.logError))
    .pipe(gulp.dest(config.buildDir));
});

/**
 * minify all css files (including the generated ones)
 */
gulp.task('minifycss', function () {
    gulp.src(config.buildDir + "/styles/*.css")
        .pipe(minifyCSS())
        .pipe(gulp.dest(config.buildDir + "/stylesmin/"));
});

/**
 * Build the project.
 */
gulp.task("build", [
  'compile',
  'resources',
  'libs',
  'sass',
  'css',
  'bower:libs'], () => {
    console.log("Building the project ...");
  });
