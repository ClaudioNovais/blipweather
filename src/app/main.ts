import { AppModule } from './app.module';
// Dynamic browser [Dev mode]
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

/**
 * This Method starts the application.
 */
platformBrowserDynamic().bootstrapModule(AppModule);
