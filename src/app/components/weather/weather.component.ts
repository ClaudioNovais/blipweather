import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
import { WeatherService } from './weather.service';
import { Weather } from './weather';
import { NotificationsService } from "notifications";

const errorTitleMsg = 'Problem gathering weather information';

@Component({
  selector: 'weather',
  templateUrl: './app/components/weather/weather.html',
  providers: [
    WeatherService, NotificationsService
  ]
})

export class WeatherComponent implements OnChanges {

  @Input() place: string;

  weather: Weather;
  errorMessage: string;

  constructor(private _WeatherService: WeatherService, private _service: NotificationsService) {

  }

  /**
   * From the OnChanges interface, this method is executed by the Change Detection system.
   * Not only when there are a change in the input field, but also at startup.
   * @param {Object} changes - An object that has the value that came as an input of this components
   */
  ngOnChanges(changes: { [propName: string]: SimpleChange }) {
    this.place = changes["place"].currentValue;
    if (this.place)
      this.getWeather(this.place);
  }

  /**
   * This method asks for new information about the place name that has received.
   * It uses a method from Weather.Service that retrieves an Observable. When the async 
   * information arrives, it updates quickly in the DOM
   * @param {String} changes - The text the user put in the inputfield. 
   */
  getWeather(place: string): void {
    this._WeatherService.getWeather(place)
      .subscribe(
      data => {
        if (data["error"]) {
          this._service.error(errorTitleMsg, data["error"]);
        }else{
          this.weather = data;
        }
      },
      error => this._service.error(errorTitleMsg, error)
      )
  }

  /**
   * Options for NotificationService. This Should be something generic and in only one place.
   */
  public options = {
    timeOut: 5000,
    lastOnBottom: true,
    clickToClose: true,
    maxLength: 0,
    maxStack: 7,
    showProgressBar: true,
    pauseOnHover: true,
    preventDuplicates: false,
    preventLastDuplicates: "visible",
    rtl: false,
    animate: "scale",
    position: ["right", "bottom"]
  };
}