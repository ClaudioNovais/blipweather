import { YahooService } from '../common/yahoo.service';
import { Weather } from './weather';
import { WeatherDay } from './weatherDay';
import { Location } from './../common/location';
import { Wind } from './wind';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';


@Injectable()
export class WeatherService extends YahooService {
  public units = 'c';
  public zeroResultsMsg: string = 'There are no weather data availble for this place';

  constructor(public http: Http) {
    super(http);
  }

  /**
   * Execute a http request to yahoo weather api 
   * @param {string} place - The query (ie city or country) que user filled in navbarbox
   * @return {Observable} - The Weather async object 
   */
  public getWeather = (place: string): Observable<Weather> => {
    let query = `select * from weather.forecast where u='${this.units}' and woeid in (select woeid from geo.places(1) where text="${place}")`;
    
    return super.executeQuery(query);
  }

  /**
   * Execute a http request to yahoo weather api 
   * @param {object} jason - The resultset retrieved by yahoo API and converted in a JSON object.
   * @return {Weather} - A new object Weather 
   */
  transformData(json: any): Weather {
    let channel = json["results"]["channel"];

    //for location stuff
    let location = this.createLocation(channel);
    //for wind stuff
    let wind = this.createWind(channel);

    // For todays forecast (this should validate if first element is today)
    let forecastDataArray: Object[] = channel["item"]["forecast"];
    let temperatureUnit = channel["units"]["temperature"]
    let today = channel["item"]["condition"];
    let weatherToday = this.createWeatherDay(today, temperatureUnit);

    // for other days:
    let otherDays: WeatherDay[] = new Array<WeatherDay>();
    for (let forecastData of forecastDataArray) {
      otherDays.push(this.createWeatherDay(forecastData, temperatureUnit));
    }

    // lastBuildDate
    let lastBuildDate = channel["lastBuildDate"];

    return new Weather(
      location,
      otherDays,
      channel["title"],
      weatherToday,
      wind,
      lastBuildDate
    );
  }

 /**
   * Create a WeatherDay object, through a json object 
   * @param {object} json - Part of resultset retrieved by yahoo API, about Weather, and converted in a JSON object.
   * @param {String} temperatureUnit - The temperature unit that will be displayed (can be C or F). 
   * @return {Weather} - A new object Weather 
   */
  private createWeatherDay(json: Object, temperatureUnit: string): WeatherDay {
    return new WeatherDay(
      json["code"],
      json["date"],
      json["day"],
      json["high"] || json["temp"],
      json["low"],
      temperatureUnit,
      json["text"]);
  }

  /**
   * Create a Wind object, through a json object 
   * @param {object} json - Part of resultset retrieved by yahoo API, wind, and converted in a JSON object.
   * @return {Wind} - A new object Wind 
   */
  private createWind(json: Object): Wind {
    return new Wind(
      json["wind"]["chill"],
      json["wind"]["direction"],
      json["wind"]["speed"],
      json["units"]["speed"]
    );
    
  }


  /**
   * Create a Location object, through a json object 
   * @param {object} json - Part of resultset retrieved by yahoo API, Location, and converted in a JSON object.
   * @return {Location} - A new object Location 
   */
  private createLocation(json: Object): Location {
    return new Location(
      json["location"]["city"],
      json["location"]["region"],
      json["location"]["country"],
      null
    );
  }
}

