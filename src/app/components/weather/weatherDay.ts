/**
 * Class used to gather the weather of one day. It's part of the Weather object.
 */
export class WeatherDay {
    public date: Date;
    public lowWUnit: string;
    public highWUnit: string;
    public imageUrl: string;
    public hslTempColor: string;

    constructor(
        private _code: number = 3200,
        private _date: string,
        public day: string,
        public high: string,
        private _low: string,
        public tempUnits: string,
        public text: string
    ) {

        /* This is only a quicker way to materialize processing*/ 
        this.imageUrl = `images/icons/${_code}.png`;
        this.date = new Date(_date);
        this.lowWUnit = `${_low}${(_low) ? ` º${tempUnits}` : ""}`;
        this.highWUnit = `${high}${(high) ? ` º${tempUnits}` : ""}`;
        this.hslTempColor = this.getHSLcolor(Number(this.high));
    }

    /**
     * This method receives a temperature value and returns a string 
     * used as a background style of that zone that shows the Weather information.
     * 
     * @param {number} - The temperature value.
     * @return {String} - The color characteristics that will be consumed by AngularJS. 
     */
    getHSLcolor(t: number): string {
        /* H needs to be from 190 to 26, so there are 164 possible values.
         Temperature will be from -5 to 40, so there are 45 different
         values. So 45*3 = 135, therefor
         */
        
        let h= 190 -(t*4); 

        return `hsl(${h}, 68%, 48%)`;
    };
}