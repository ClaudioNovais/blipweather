

/**
 * Class used to gather the wind information to be inside the Weather object.
 */
export class Wind {
    public speed: string;

    constructor(
        public chill    : string,
        public direction: string,
        private _speed  : string,
        private _speedUnit : string 
    ) {
        /* This is necessary because we don't know if it is Celsius or fahrenheit */
        this.speed= `${_speed}${(_speed)?`${_speedUnit}`:""}`;
    }

    
}