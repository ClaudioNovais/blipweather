import { WeatherDay } from './weatherDay';
import { Wind } from './wind';
import { Location } from './../common/location';

export class Weather {

    constructor(
        public location: Location,
        public otherDays: WeatherDay[] = [],
        public title: string,
        public today: WeatherDay,
        public wind: Wind,
        public lastBuildDate: Date
    ) {

    }
}
