import { Log } from './logger';
import { Http, Response, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

const _consumerKey: string = 'dj0yJmk9bU9hTW1ZUmtvdk5rJmQ9WVdrOVYyTlFVa0pDTm1jbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD1hMA--';
const _consumerSecret: string = 'b4f4a7c3174d4863def3733c460628ab4f6ca812';


/**
 * This is an abstract class for yahoo YQL requests. Alone won't do much,
 * but its existence makes the code less duplicated.
 */
@Injectable()
export class YahooService {

    public zeroResultsMsg: string = 'There are no data availble for this query';
    format: string = '&format=json';
    headers: Headers = new Headers();


    /**
     * This constructor will create the basic http headers and gathers the http service.
     * @param {object} - The http service used to make calls;
     */
    constructor(public http: Http) {
        this.setHeaders({});
    }

    
    /**
     * This method executes a query in YQL, the yahoo language to make calls to its API.
     * @param {String} - The Query to be passed.
     */
    executeQuery(query: string): Observable<any> {

        if (!query) Observable.throw('A query must be given!');

        let calllingUrl = `https://query.yahooapis.com/v1/public/yql?q=${query}${this.format}`;

        Log.print(this, `[calling....] ${calllingUrl}`);

        return this.http.get(calllingUrl, this.headers)
            .map((r: Response) => {
                let resultJson = r.json().query;

                if (resultJson.count===0){
                    return this.throwError(this.zeroResultsMsg);
                }else{
                    Log.yellow(this,resultJson);
                    return this.transformData(resultJson);    
                }
            })
            .catch(this.handleError);
    }

    handleError(error: Response): Observable<any> {
        return this.throwError(error.json().error || 'Server error');
    }

    throwError(error: String): Observable<any> {
        return Observable.throw(error);
    }

    transformData(json: any): any {
        return json;
    }

    setHeaders(headers: { [index: string]: string; }): void{
        this.headers = new Headers();
        this.headers.append('consumerSecret', _consumerSecret);
        this.headers.append('tokenSecret', _consumerKey);
        this.headers.append('lang', 'pt-PT');
        for (var key in headers) {
            var value = headers[key];
        }
    }
}

