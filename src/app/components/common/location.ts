
export class Location {

    constructor(
        public city: string,
        public region: string,
        public country: string,
        public woeid:string
    ) {

    }
}
