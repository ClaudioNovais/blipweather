const devMode = true;

export class Log {

    public static yellow(instance: any, message: any): void {
        this.print(instance,message,"yellow");
    }
    public static white(instance: any, message: any): void {
        this.print(instance,message,"white");
    }

    public static print(instance: any, message: any, color: string ="white"): void {
     if (devMode){
            console.log(`%c ${this.getType(instance)}.class calling...`, `background: ${color}; color: black`);
            console.log(message);
        }
    }


    private static getType(instance: any): string {

        var funcNameRegex = /function (.{1,})\(/;
        var results = (funcNameRegex).exec((instance).constructor.toString());
        return (results && results.length > 1) ? results[1] : "";
    }
}


/**
 *  GetType function biography:
 * http://stackoverflow.com/questions/22777181/typescript-get-to-get-class-name-at-runtime
 * 
 * Note that there are problems in Javascript when asking for 
 * the constructor, but this should only be used in devmode, 
 * so no worries (for now :).
 * http://stackoverflow.com/questions/332422/how-do-i-get-the-name-of-an-objects-type-in-javascript
 * 
 */