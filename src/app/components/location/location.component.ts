import { Component, Input, EventEmitter, Output } from '@angular/core';
import { LocationService } from './location.service';
import { Location } from './../common/location';
import { NotificationsService } from "notifications";

const errorTitleMsg="Cannot find that place";
@Component({
  selector: 'location',
  templateUrl: './app/components/location/location.html',
  providers: [
    LocationService, NotificationsService
  ]
})

export class LocationComponent {
  
  public location: Location;
  errorMessage: string;
  placeholder: string = "Type a place & press enter";

  @Output() place: EventEmitter<any> = new EventEmitter()

  constructor(private _locationService: LocationService,  private _service: NotificationsService ) { 
    
  }

  /**
   * The event that will make the Change Detection after the user clicks In
   * the Enter key. This method executs the getLocation and then that method will
   * tell the system to be updated
   * 
   * @param {String} Place - The text in the navbar input. 
   */
  sendOutEvent(box){
    let newplace = box['target'].value;
    if (newplace){
      this.getLocation(newplace);
    }
  }

  

  getLocation(location: string): void{
    this._locationService.getLocation(location)
                        .subscribe(
                          data  => {
                            if (data["error"]){
                              this._service.error(errorTitleMsg, data["error"]);
                            }else{
                              this.location = data;
                              this.place.emit(location);
                            }
                          },
                          error => this._service.error(errorTitleMsg, error)
                        )
  }

  /**
   * options for NotificationService
   */
   public options = {
        timeOut: 5000,
        lastOnBottom: true,
        clickToClose: true,
        maxLength: 0,
        maxStack: 7,
        showProgressBar: true,
        pauseOnHover: true,
        preventDuplicates: false,
        preventLastDuplicates: "visible",
        rtl: false,
        animate: "scale",
        position: ["right", "bottom"]
    };
}