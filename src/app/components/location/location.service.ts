import { YahooService } from '../common/yahoo.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Location } from './../common/location';


@Injectable()
export class LocationService extends YahooService {

  public zeroResultsMsg: string = 'There are no data availble for this search';
  constructor(public http: Http) {
    super(http);
  }

  /**
   * Get the detailed information about the place the user is asking; This will
   * be used as key for the localstorage lightweight data base;
   * 
   * @param {String} Place - The place the user is asking
   * @return {Observable} - An Observable with the object Location. 
   */
  public getLocation = (place: string): Observable<Location> => {
    let query = `select * from geo.places(1) where text="${place}"`;
    
    return super.executeQuery(query);

  }

  /**
   * This method creates a Location object with the json passed from the Yahoo API.
   */
  transformData(json: any): Location {
    let location: Object = json["results"]["place"];

    let city: string = location["name"];
    let region: string = location["name"];
    let country: string = location["country"]["content"];
    let woeid:string = location["woeid"];

    return new Location(city,region,country,woeid);
  }
 
}

