import { Component } from '@angular/core';
import { WeatherComponent } from './components/weather/weather.component';
import { LocationComponent } from './components/location/location.component';
// Add the RxJS Observable operators need for this app:
import './rxjs-operators';

@Component({
  selector: "app",
  directives: [
    WeatherComponent,
    LocationComponent
    ],
  templateUrl: './app/app.html'
})


/**
 * This is the more important component in this project. All the events,
 * from change detection, come here because it's here where we put global logic
 * if necessary.
 * 
 * @place - This is the method that commes from the LocationComponent and goes
 * to the WeatherComponent right after it's change. The GlobalData object is
 * necessary, because a String in Java script is imutable.
 * 
 */
export class AppComponent {
  globalData: Object = {
    place: ""
  }


/**
 * The method from where the variable place comes (from the LocationComponent).
 * When we make the association present in this method, de variable refference isn't
 * updated, so the Change Detection will tell WeatherComponent right after this
 * execution of this method.
 * 
 * @param {object} - The text the user put in the input box
 */
  handleEvent(place) {
    this.globalData["place"] = place;
  }
}
