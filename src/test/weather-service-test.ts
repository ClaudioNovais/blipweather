import {it, describe, expect, beforeEachProviders, inject} from "@angular/core/testing";
import {Response, XHRBackend, ResponseOptions, HTTP_PROVIDERS} from '@angular/core/http';
import {MockConnection, MockBackend} from "angular2/src/http/backends/mock_backend";
import {provide} from "angular2/core";



import {WeatherService} from "./../app/components/weather/weather.service";
describe('WeatherService Tests', () => {
    beforeEachProviders(() => {
        return [
            HTTP_PROVIDERS,
            provide(XHRBackend, {useClass: MockBackend}),
            WeatherService
        ]
    });

    it('Should create a component MyList',
        inject([XHRBackend, WeatherService], (backend, service) => {
            backend.connections.subscribe(
                (connection:MockConnection) => {
                    var options = new ResponseOptions(getNormalResponse());
                    var response = new Response(options);
                    connection.mockRespond(response);
                }
            );

            service.getWeather("Porto").subscribe(
                (weather) => {
                    expect(weather.location.city).toBe('Porto');
                }
            );

        })
    );
});


function getNormalResponse(){
  return '{"query":{"count":1,"created":"2016-08-20T06:55:55Z","lang":"pt","results":{"channel":{"units":{"distance":"km","pressure":"mb","speed":"km/h","temperature":"C"},"title":"Yahoo! Weather - Porto, Porto, PT","link":"http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-746203/","description":"Yahoo! Weather for Porto, Porto, PT","language":"en-us","lastBuildDate":"Sat, 20 Aug 2016 07:55 AM WEST","ttl":"60","location":{"city":"Porto","country":"Portugal","region":" Porto"},"wind":{"chill":"50","direction":"0","speed":"11.27"},"atmosphere":{"humidity":"94","pressure":"34202.54","rising":"0","visibility":"19.31"},"astronomy":{"sunrise":"6:50 am","sunset":"8:24 pm"},"image":{"title":"Yahoo! Weather","width":"142","height":"18","link":"http://weather.yahoo.com","url":"http://l.yimg.com/a/i/brand/purplelogo//uh/us/news-wea.gif"},"item":{"title":"Conditions for Porto, Porto, PT at 07:00 AM WEST","lat":"41.14938","long":"-8.61034","link":"http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-746203/","pubDate":"Sat, 20 Aug 2016 07:00 AM WEST","condition":{"code":"32","date":"Sat, 20 Aug 2016 07:00 AM WEST","temp":"11","text":"Sunny"},"forecast":[{"code":"32","date":"20 Aug 2016","day":"Sat","high":"23","low":"12","text":"Sunny"},{"code":"32","date":"21 Aug 2016","day":"Sun","high":"30","low":"12","text":"Sunny"},{"code":"32","date":"22 Aug 2016","day":"Mon","high":"28","low":"16","text":"Sunny"},{"code":"30","date":"23 Aug 2016","day":"Tue","high":"25","low":"14","text":"Partly Cloudy"},{"code":"34","date":"24 Aug 2016","day":"Wed","high":"23","low":"15","text":"Mostly Sunny"},{"code":"30","date":"25 Aug 2016","day":"Thu","high":"22","low":"15","text":"Partly Cloudy"},{"code":"34","date":"26 Aug 2016","day":"Fri","high":"23","low":"13","text":"Mostly Sunny"},{"code":"34","date":"27 Aug 2016","day":"Sat","high":"25","low":"13","text":"Mostly Sunny"},{"code":"34","date":"28 Aug 2016","day":"Sun","high":"24","low":"14","text":"Mostly Sunny"},{"code":"34","date":"29 Aug 2016","day":"Mon","high":"23","low":"15","text":"Mostly Sunny"}],"description":"<![CDATA[<img src=\"http://l.yimg.com/a/i/us/we/52/32.gif\"/>\n<BR />\n<b>Current Conditions:</b>\n<BR />Sunny\n<BR />\n<BR />\n<b>Forecast:</b>\n<BR /> Sat - Sunny. High: 23Low: 12\n<BR /> Sun - Sunny. High: 30Low: 12\n<BR /> Mon - Sunny. High: 28Low: 16\n<BR /> Tue - Partly Cloudy. High: 25Low: 14\n<BR /> Wed - Mostly Sunny. High: 23Low: 15\n<BR />\n<BR />\n<a href=\"http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-746203/\">Full Forecast at Yahoo! Weather</a>\n<BR />\n<BR />\n(provided by <a href=\"http://www.weather.com\" >The Weather Channel</a>)\n<BR />\n]]>","guid":{"isPermaLink":"false"}}}}}}'
}